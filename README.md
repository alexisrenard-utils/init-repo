# Information

This base aims to initialize a repo the proper way : make releases & pre-commit.

## Makefile

This repository use Makefile to manage easily module version.

**Warning** : Don't tag manually.

### Usage

```bash
> make help

default         Default Task, build program with default values
version         Get current version
check-status    Check current git status
check-release   Check release status
major-release   Do a major-release, ie : bumped first digit X+1.y.z
minor-release   Do a minor-release, ie : bumped second digit x.Y+1.z
patch-release   Do a patch-release, ie : bumped third digit x.y.Z+1
help            Show this help (Run make <target> V=1 to enable verbose)
```

## Pre-commit
> TODO

## Gitmoji
The commits had been done following the gitmoji [guidelines](https://gitmoji.carloscuesta.me/).
